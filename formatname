#!/usr/bin/env bash
# #############################################################################
# Simple bash script to remove unwanted chars from names
# copyright (C) 2014 Olivier LEMAIRE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# #############################################################################
echo "remove spaces, commas and stuff that should not be in file names"

function usage (){
# this should be enhanced
printf "formatname filename \n" 
}

# 
# author    Christophe Blaess 
# link      http://www.blaess.fr/christophe/livres/scripts-sous-linux/ 
#
function latin_to_ascii {
    local result
    result=$(echo "$1" |
    sed -e 's/[ÀÁÂÃÄÅ]/A/g' |
    sed -e 's/Æ/AE/g'       |
    sed -e 's/Ç/C/g'        |
    sed -e 's/[ÈÉÊË]/E/g'   |
    sed -e 's/[ÌÍÎÏ]/I/g'   |
    sed -e 's/Ñ/N/g'        |
    sed -e 's/[ÒÓÔÕÖØ]/O/g' |
    sed -e 's/[ÙÚÛÜ]/U/g'   |
    sed -e 's/Ý/Y/g'        |
    sed -e 's/[àáâãä]/a/g'  |
    sed -e 's/æ/ae/g'       |
    sed -e 's/ç/c/g'        |
    sed -e 's/[èéêë]/e/g'   |
    sed -e 's/[ìíîï]/i/g'   |
    sed -e 's/ñ/n/g'        |
    sed -e 's/[òóôöø]/o/g'  |
    sed -e 's/[ùúûü]/u/g'   |
    sed -e 's/ý/y/g')

    echo "$result"
}

function remove_chars () {
    # get the basename -- shortest occurence of a dot followed with whatever
    # removes spaces from the variable 
    local result
    result=$(echo "$1" | tr '[:blank:]' '_')
    # local result=${1//\ /_}

    # get the basename with leading ./ stripped
    result=${result##*./}

    # get the name without extension
    if [ ! -d "$1" ]; then
        echo "$1 is not a directory"
        result=${result%\.*}
        local extension=.${1##*.}
    else
        echo "$1 is a directory"
        extension=""
    fi 
    
    # strip any punctuation sign 
    result=$(echo "$result" | tr -s '[:punct:]' '_')
    result=$(echo "$result" | tr -s '°' '_')

    # remove accentuated chars 
    result=$(latin_to_ascii "$result")
    result=${result%_}
    result=${result#_}

    REPLY=$result$extension
    echo "$REPLY"
}


# main part of the script 
if (( $# < 1 ))
then
    usage
    exit
fi
# check if some options have been passed
while getopts l name 
do
    case $name in 
        l) lopt=1;;
        *) echo "[WARNING] Invalid option";;
    esac
done 

# Shift off the options and optional --.
# we need to remove the options from the list 
shift "$((OPTIND-1))" 


for file in "$@"
do 
    # remove special chars 
    remove_chars "$file"
    destname=$REPLY 

    if [[ ! -z $lopt ]]
    then 
        destname="$(echo "$destname" | tr '[:upper:]' '[:lower:]')"
    fi 

    if [ ! -e "$destname" ]; then
        echo "$file" renamed to "$destname"
        mv -- "$file" "$destname"
    else
        echo "$file" not renamed
    fi
done
